import { Component, OnInit } from '@angular/core';
import {user} from './user'
import { NgForm } from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module'
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit{
title:string='User';
USER: user = {} as user;
createUser:boolean=false;  
showform:boolean=false;
editUser:boolean=false
saveUser: user = {} as user;
constructor() { }
onclick(){
  this.createUser=!this.createUser
}

onCreate(useform){
console.log(this.USER) 
Object.assign(this.saveUser,this.USER)
this.showform=!this.showform;
useform.reset()
}
onEdit(){
  this.editUser=!this.editUser
}

ngOnInit() {
  }

}
