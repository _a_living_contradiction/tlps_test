import { NgModule } from '@angular/core';
import {RouterModule, Routes, Route} from '@angular/router';
import {UserComponent} from './user/user.component';
import {TowersComponent} from './towers/towers.component';
import {InspectionComponent} from './inspection/inspection.component';
import {VendorsComponent} from './vendors/vendors.component';
import {MapComponent} from './map/map.component'


const routes:Routes=[
//{path:' ',redirectTo:'./Users',pathMatch:'full'},
{path:'Users',component:UserComponent},
{path:'towers',component:TowersComponent},
{path:'inspection',component:InspectionComponent},
{path:'vendor',component:VendorsComponent},
{path:'map',component:MapComponent},
]


@NgModule({
  
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
  
})

export class AppRoutingModule { }
