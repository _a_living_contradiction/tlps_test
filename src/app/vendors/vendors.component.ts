import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.css']
})
export class VendorsComponent implements OnInit {
title:string='Vendor';
createVendor:boolean=false;
  constructor() { }

  ngOnInit() {
  }
  onclick(){
    this.createVendor=!this.createVendor;
  }

}
